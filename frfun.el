/*-----------------------------------------------------------------------------------------
 LIBRARY: TWOPHASETUBE
 FILE: frfun
 CREATION DATE: 22/08/2023
-----------------------------------------------------------------------------------------*/
USE MATH
USE FLUID_PROPERTIES
USE THERMO_TABLE_INTERP

ENUM OPT_FR =  {FR_Freidel}
FUNCTION REAL hdc_fric_twophase
    (
	 	 SET_OF(Chemicals_full) mix  "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
		 IN REAL x_mix[mix], 
		 IN ENUM FluidKeys fluid, 
		 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
		 IN ENUM OPT_FR option_fr "opzione calcolo coeff attrito",
	   IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
	   --geometry
	   IN REAL A					UNITS u_m2		"cross area (m^2)",
	   IN REAL Dh				UNITS u_m		"hydraulic diameter (m)",
		 IN REAL z	      		UNITS u_m      "local axial abscissa",
		 IN REAL L					UNITS u_m      "tube length",
     --fluid properties
     IN ENUM Phase phase	UNITS no_units	"Fluid phase (-)",
     IN REAL P					UNITS u_Pa		"pressure (Pa)",
     IN REAL T					UNITS u_K		"fluid temperature (K)",
     IN REAL Tsat				UNITS u_K		"saturation temperature (K)",
		 IN REAL x					UNITS no_units	"quality",
		 IN REAL alpha   UNITS no_units ,
     IN REAL hf				UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
     IN REAL hg				UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
		 IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
		 IN REAL xin				UNITS no_units "inlet quality (-)", 
		 IN REAL rho,
     IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
     IN REAL rhog				UNITS u_kg_m3	"density of vapor (kg/m^3)",
     IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
     IN REAL viscf			UNITS u_Pas		"viscosity of liquid (Pa*s)",
     IN REAL viscg			UNITS u_Pas		"viscosity of vapor (Pa*s)",
     IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
     IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
     IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
     IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
     IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
     IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
     IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
		 IN REAL rug
		 )
		 DECLS
		 	REAL Grav, reynolds,fr
			ENUM FR_OPTION option_2phase
		 BODY 
		 Grav = 9.80665
		 
		 IF (option_fr == FR_Freidel) THEN
		 		reynolds = rho*abs(m/(rho*A))*Dh/visc
				option_2phase = FR_tube
		 		fr = hdc_fric_2phase(option_2phase,Dh,rug,reynolds,m,x,alpha,rhof,rhog,viscf,viscg,sigma,Grav,Tsat)
		 ELSE
		 
		 END IF
		 
		 RETURN fr
END FUNCTION