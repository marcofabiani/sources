/*-----------------------------------------------------------------------------------------
 LIBRARY: TWOPHASETUBE
 FILE: tube_steady
 CREATION DATE: 24/03/2023
-----------------------------------------------------------------------------------------*/
USE MATH
USE PORTS_LIB
USE FLUID_PROPERTIES
USE THERMAL
USE STEADY
-------------------------------------------------------------------------------
ABSTRACT COMPONENT ABS_Tube_1D_ss_tp(
    SET_OF(Chemicals_full) BG_Option = Chemicals "Select Chemicals in normal cases, or create a particular SET_OF of chemicals for reduced burned gases",
    INTEGER nodes = 5               "Number of nodes in the pipe",
    INTEGER n_bends = 1             UNITS no_units		"Number of bends (-)",
    ENUM DesignType type = Design   "Design, Fixed dP; OffDesign, fixed flow area",
    ENUM DeltaP dp_type = UserGiven "DeltaP option"
    ) "Cylindrical 1D pipe with 1D heat transfer with the wall"

    PORTS
        IN fluid_s  (burnerGasesOption = BG_Option) f1              "Inlet fluid port"
        OUT fluid_s (burnerGasesOption = BG_Option) f2             "Outlet fluid port"
        IN thermal(n = nodes) tp_in "Thermal port"

    DATA
        REAL dP_design = 1e4				UNITS u_Pa		"Pressure drop in design mode"
        REAL dP_per = 15					UNITS "%"		"Relative pressure drop in Design mode if dp_type = Percentage (%)"
        REAL num = 1							UNITS no_units	"Number of parallel tubes"
--        ENUM INIT_OPTION init_option = INIT_PT		"Option to specify the initial thermodynamic state"
        REAL P_o =  100000.				UNITS u_Pa		"Initial pressure"
        REAL T_o =  293.15					UNITS u_K		"Initial temperature"
--        REAL rho_o = 1.					UNITS u_kg_m3	"Initial density"
        REAL m_o =  1.						UNITS u_kg_s	"Initial mass flow"
        REAL rug = 0.05e-3					UNITS u_m		"Roughness"
        REAL k_f = 1							UNITS no_units	"Multiplier of the friction factor"
        REAL alpha_bend[n_bends] = 0	UNITS u_deg		RANGE 0, 360.	"Bend angle"
        REAL R_bend[n_bends] = 1			UNITS u_m		RANGE 0, Inf	"Ratio of curvature bend"
        REAL fld_add = 0					"Additional losses in f*L/D"
--        ENUM HT_OPTION ht_option =  HT_tube "Heat transfer option - tube calculation or tank calculation or constant"
--        REAL hc_dat = 1					UNITS u_W_m2K	"Heat transfer coefficient if ht_option = Ht_constant"
			ENUM OPT_GEN option_GEN			"general option to use legacy correlations"
			ENUM COND_Option  option_cond " CONDENSATION MODEL for ht tube"
		  ENUM OPT_CHF option_CHF     "Heat transfer option - Critical heat flux"
		  ENUM OPT_DRY option_DRY     "Heat transfer option - Dryout heat flux"
		  ENUM OPT_DNB option_DNB     "Heat transfer option - DNB heat flux"
		  ENUM OPT_NUC option_NUC     "Heat transfer option - NUC heat flux"
			ENUM OPT_FR	option_fr				" friction factor option"
    DECLS
        REAL Dh[nodes]						UNITS u_m		"Diameter"
        REAL A[nodes]						UNITS u_m2		"Cross section"
        REAL V[nodes]						UNITS u_m3		"Volume"
        EXPL REAL dL[nodes]				UNITS u_m		"Cell mesh size array"
        
        REAL zeta_bends						UNITS no_units	"Loss coefficient due to the bends"
        REAL  alpha[nodes]					UNITS no_units	"Void fraction"
        REAL  cp[nodes]						UNITS u_J_kgK	"Specific heat at constant pressure"
        REAL  cpf[nodes]					UNITS u_J_kgK	"Specific heat at constant pressure of saturated liquid"
        REAL  cpg[nodes]					UNITS u_J_kgK	"Specific heat at constant pressure of saturated vapor"
        REAL  cond[nodes]					UNITS u_W_mK	"Fluid conductivity" 
        REAL  condf[nodes]					UNITS u_W_mK	"Fluid conductivity of liquid" 
        REAL  condg[nodes]					UNITS u_W_mK	"Fluid conductivity of vapor" 
        REAL  vsound[nodes]				UNITS u_m_s		"Sound speed"
        REAL  DP[nodes]						UNITS u_Pa		"Pressure drop"
        REAL  drho_dp[nodes]				UNITS "s^2/m^2"		"drho/dp at constant h (s^2/m^2)"
        REAL  drho_dh[nodes]				UNITS "kg*s^2/m^5"	"drho/dh at constant p (kg*s^2/m^5)"
        REAL  fr[nodes]						UNITS no_units	"Friction factor"
        ALG REAL  h[nodes]					UNITS u_J_kg	"Global total Enthalpy"
        REAL  hf[nodes]						UNITS u_J_kg	"Enthalpy of saturated liquid"
        REAL  hg[nodes]						UNITS u_J_kg	"Enthalpy of saturated vapor"
        EXPL REAL  hc[nodes]				UNITS u_W_m2K	"Internal film coefficient"
        REAL  Mach[nodes]					UNITS no_units	"Mach number=v/vsound"
        ALG REAL  P[nodes]					UNITS u_Pa		"Pressure in the volumes"
        REAL  q[nodes]						UNITS u_W		"Heat flow"
        REAL  Re[nodes]						UNITS no_units	"Reynolds number"
        REAL  rho[nodes]					UNITS u_kg_m3	"Global density"
        REAL  rhof[nodes]					UNITS u_kg_m3	"Density of saturated liquid"
        REAL  rhog[nodes]					UNITS u_kg_m3	"Density of saturated vapor"
        REAL  sigma[nodes]					UNITS u_N_m		"Surface tension"
        REAL  T[nodes]						UNITS u_K		"Temperature in the volume"
        REAL  Tsat[nodes]					UNITS u_K		"Saturation temperature in volume"
        REAL  u[nodes]						UNITS u_J_kg	"Global total energy"
        REAL  vel[nodes]					UNITS u_m_s		"Average velocity in continuity node"

        REAL  visc[nodes]					UNITS u_Pas		"Viscosity"
        REAL  viscf[nodes]					UNITS u_Pas		"Viscosity of saturated liquid"
        REAL  viscg[nodes]					UNITS u_Pas		"Viscosity of saturated vapor"
        REAL  x[nodes]						UNITS no_units	"Vapour mass fraction"
        REAL  zeta[nodes]					UNITS no_units	"Equivalent distributed friction"

        ALG REAL  m							UNITS u_kg_s	"Mass flow in the pipe"
        ENUM  Phase phase[nodes]			"Phase of the fluid in each node"

        INTEGER  ier[nodes]=0				"Error index of thermodynamic function calls"
        INTEGER  ipx[nodes]				"Last index for x interpolation"
        INTEGER  ipy[nodes]				"Last index for y interpolation"
        INTEGER  ier1						"Error index of critical flow function call"
        PRIVATE DISCR REAL x_nc[nodes]	UNITS no_units	"Non-condensable mass fraction"
        PRIVATE REAL P_nc[nodes]			UNITS u_Pa		"Non-condensable partial pressure"

        REAL dP								UNITS u_Pa		"Pipe Pressure drop"
		  	REAL flag[nodes]					UNITS no_units "flag for regime identification"
				REAL dTw[nodes]
    INIT
        Init_Vol(BG_Option, f1.x_eq, f1.fluid, NoFluid, INIT_PT, 0., P_o, T_o, 0, 0, 
           P[1], P_nc[1], x_nc[1], u[1], T[1], rho[1], vsound[1], x[1], alpha[1], visc[1], cond[1], cp[1], ipx[1], ipy[1], ier[1])

        f1.P = P_o 
        f2.P = P_o -- - dP_design
        f1.h = u[1] + P_o/rho[1]
        f2.h = f1.h

        FOR (i IN 1,nodes)
           P[i] = P_o
           T[i] = T_o
           tp_in.Tk[i] = T_o
           rho[i] = rho[1]
           u[i] = u[1]
           h[i] = u[1] + P[1]/rho[1]
           DP[i] = dP_design/nodes
        END FOR
        m = m_o/num

    DISCRETE
        
    CONTINUOUS
        f1.x_eq = f2.x_eq
        f1.fluid = f2.fluid
        f1.n_fluid = f2.n_fluid

        -- Conservation of Momentum and Energy (centred scheme)
        EXPAND_BLOCK (i IN 1, nodes-1)
            P[i+1] = P[i] - (DP[i]+DP[i+1])/2
            h[i+1] = h[i] + (q[i] + q[i+1])/max(1e-6,m)/2
        END EXPAND_BLOCK

        --Pressure and enthalpy to the ports
--SEQUENTIAL
        f1.P = P[1] + DP[1]/2   
        f2.P = P[nodes] - DP[nodes]/2
--END SEQUENTIAL
        h[1] = f1.h +  q[1]/max(1e-6,m)/2
        f2.h = h[nodes] + q[nodes]/max(1e-6,m)/2

        -- Thermodynamic and transport properties
        EXPAND_BLOCK (i IN 1, nodes)
            FL_state_vs_ph(BG_Option, f1.x_eq, f1.fluid, NoFluid, P[i], h[i], 0., P_nc[i], phase[i], rho[i], u[i], \
               rhof[i], rhog[i], T[i], Tsat[i], hf[i], hg[i], \
               x[i], alpha[i], cp[i], cpf[i], cpg[i], drho_dp[i], drho_dh[i], vsound[i], \
               visc[i], viscf[i], viscg[i], cond[i], condf[i], condg[i], sigma[i], 1, ipx[i], ipy[i], ier[i])   
SEQUENTIAL
            -- Auxiliary variables calculation (friction, Reynolds, film coef., etc)
            vel[i]  = m / rho[i] / A[i]
            Mach[i] = vel[i] / vsound[i] 
            Re[i]   = abs(m) / A[i] * Dh[i] / visc[i]
--            fr[i]   = hdc_fric(Dh[i], rug, Re[i])
						fr[i] = hdc_fric_twophase(BG_Option, f1.x_eq,f1.fluid,NoFluid, option_fr, m, A[i], Dh[i], dL[1]*i	, dL[1]*nodes,\
						phase[i], P[i]	, T[i], Tsat[i], x[i],alpha[i], hf[i], hg[i], h[i],(h[1]-hf[1])/(hg[1]-hf[1]), rho[i], rhof[i], rhog[i], visc[i], viscf[i], viscg[i], cond[i], condf[i],condg[i],\
						cp[i],cpf[i],cpg[i],sigma[i],rug)
						
            zeta[i] = fld_add/nodes + zeta_bends/nodes + k_f*fr[i]*dL[i]/Dh[i]
--            hc[i] = htc_tube_sp(m, A[i], Dh[i], cond[i], visc[i], cp[i])
--            hc[i] = htc_fun(ht_option, hc_dat, m, 0., A[i], Dh[i], phase[i], \
--                    P[i], T[i], Tsat[i], hf[i], hg[i], rho[i], rhof[i], rhog[i], x[i], visc[i],viscf[i],viscg[i], \
--                    cond[i],condf[i],condg[i], cp[i],cpf[i],cpg[i], sigma[i], tp_in.Tk[i], GRAV)
						hc[i] =  htc_tube_twophase(BG_Option, f1.x_eq,f1.fluid,NoFluid,option_GEN,option_cond,option_CHF,option_DRY,option_DNB,option_NUC, m, A[i], Dh[i], dL[1]*i	, dL[1]*nodes,\
						phase[i], P[i]	, T[i], Tsat[i], x[i], hf[i], hg[i], h[i],(h[1]-hf[1])/(hg[1]-hf[1]), rho[i], rhof[i], rhog[i], visc[i], viscf[i], viscg[i], cond[i], condf[i],condg[i],\
						cp[i],cpf[i],cpg[i],sigma[i],tp_in.Tk[i],q[i]/(PI*Dh[i]*dL[i]),flag[i],dTw[i])
END SEQUENTIAL
            -- Pressure drop calculated
            DP[i] = 0.5 * zeta[i] * fpow(m/A[i],0,2) / rho[i]
        END EXPAND_BLOCK

        -- mass flow at the ports
        f1.m = num * m
        f2.m = f1.m

        dP = f1.P - f2.P
END COMPONENT

-------------------------------------------------------------------------------
COMPONENT Tube_ss_tp IS_A ABS_Tube_1D_ss_tp \
    "Cylindrical 1D fluid vein with 1D heat transfer with the wall"

    PORTS

    DATA
        REAL L = 1.0						UNITS u_m		"Pipe length"
        REAL D = 0.01					UNITS u_m		"Nominal pipe inner diameter"

    DECLS
        DISCR REAL SR_bend[n_bends]=1	UNITS no_units	"Side ratio of bend cross section"
        ALG REAL D_cal						UNITS u_m		"Pipe diameter [calculated in design mode]"

    INIT
        	D_cal = D
        	FOR(i IN 1,nodes)
		      Dh[i]=D
         END FOR

    CONTINUOUS
        -- geometrical mesh construction
        EXPAND_BLOCK (i IN 1, nodes)
            dL[i] = L/nodes
            Dh[i] = D_cal
            A[i]  = 0.25 * PI * Dh[i]**2
            V[i]  = A[i]*dL[i]
        END EXPAND_BLOCK

        zeta_bends =  SUM(j IN 1, n_bends; hdc_k_bend(Circular, alpha_bend[j], \
            R_bend[j]/Dh[1], rug/Dh[1], SR_bend[j], 1e5))

        --  Heat transfer (one port)
        EXPAND(i IN 1, nodes) q[i] = hc[i] * PI*Dh[i]*dL[i] * (tp_in.Tk[i] - T[i])
        EXPAND(i IN 1, nodes) tp_in.q[i] = num * q[i]

        -- Desig/off-design equations
        IF (type == Off_Design) INSERT
            D_cal = D
        ELSE
            IF(dp_type == UserGiven)  INSERT
               IMPL(D_cal) dP = dP_design
            ELSEIF(dp_type == Percentage) INSERT
               IMPL(D_cal) dP = dP_per * f2.P / 100
            END IF
        END IF
END COMPONENT