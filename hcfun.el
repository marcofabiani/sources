/*-----------------------------------------------------------------------------------------
 LIBRARY: TWOPHASETUBE
 FILE: hcfun
 CREATION DATE: 14/02/2023
 AUTHORS : M.F.^2
-----------------------------------------------------------------------------------------*/
USE MATH
USE FLUID_PROPERTIES
USE THERMO_TABLE_INTERP
--USE FLUID_FLOW_1D

ENUM OPT_GEN =  {HT_tube_orig, HT_boil_orig, newmodel}
ENUM OPT_CHF =  {CHF_Ganesan, CHF_Shah} "Heat transfer option - Critical heat flux" 
ENUM OPT_DRY =  {DRY_Ganesan, 						        DRY_Hendricks, DRY_bromley, DRY_bromleymod, DRY_miropolskii, DRY_groeneveld} "Heat transfer option - Dryout heat flux" 
ENUM OPT_DNB =  {DNB_Ganesan_eq, DNB_Ganesan_neq, DNB_Hendricks, DNB_bromley, DNB_bromleymod, DNB_miropolskii, DNB_groeneveld} "Heat transfer option - Dryout heat flux" 
ENUM OPT_NUC =  {NUC_Chen} "Heat transfer option - Nucleate heat flux" 


-- Function computes the critical temperature
 FUNCTION REAL Tcr_comp
 (
	IN ENUM FluidKeys fluid		"Working fluid"
 )
 DECLS
 	REAL Tcrit
 	INTEGER ig
	INTEGER ier
	REAL prop_crit[20]			"Fluid properties at critical point"
 BODY
 			FL_table_read(fluid, ig)
			crit_cond  (ig, prop_crit, ier)
			Tcrit = prop_crit[2]
 
 RETURN Tcrit
 END FUNCTION

FUNCTION NO_TYPE prop_vs_T
(
	IN ENUM FluidKeys fluid	"Working fluid",
	IN REAL T						"Temperature",
	IN REAL p						"pressure",
	OUT REAL rho "pene",
	OUT REAL visc,
	OUT REAL cp,
	OUT REAL cond
)
DECLS
	REAL prop[3,20] 				"properties"
	REAL xfic      				"quality"
	INTEGER ier1   				"error flag"
	INTEGER INTERP_ORDER = 1   "Interpolation order"
	INTEGER ipx,ipy 				"pressure and temperature pointers in table"
	INTEGER ig 						"fluid identification number"
   

BODY
	-- find fluid identification number
	FL_table_read(fluid, ig)
	-- read thermo tables
	thermo_prop(ig, 1, p, 2, T, 111, prop, xfic, ier1, INTERP_ORDER, ipx, ipy)

/*
   rhof   = prop[2,3]
   cpf    = prop[2,11]
   viscf  = prop[2,20]
   condf  = prop[2,19]


   rhog   = prop[3,3]
   cpg    = prop[3,11]
   viscg  = prop[3,20]
   condg  = prop[3,19]
*/
	rho 	= prop[1,3]
   cp    = prop[1,11]    
   visc  = prop[1,20]
   cond  = prop[1,19]

END FUNCTION





FUNCTION REAL NRxa 
	(IN REAL x0,
	 IN REAL c[6],
	 IN REAL Frfo
	 )
DECLS
	REAL f,df,x,xint,tol,err
	INTEGER iter, itermax
	
BODY

	tol = 10**(-3)
	itermax = 10
	 x = x0
	 iter = 0
	 err = 10
	 WHILE ((iter < itermax) AND (err>tol))
	 
	 	f = (c[1]+c[2]*x+c[3]*x**2+c[4]*x**3)*Frfo**c[5] - x
	 	df = (c[2]+2*c[3]*x+3*c[4]*x**2)*Frfo**c[5] - 1
	 
	 	x = x - f/df
		err = abs(f/df)
	 	--PRINT("error=$err")

		iter = iter + 1
	END WHILE
xint = x
--PRINT("XINT=$xint")
RETURN xint
END FUNCTION


FUNCTION REAL htpa
	(	
	 SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration"
	 )

DECLS
	REAL c[6]
	REAL Frfo
	REAL xa, xint, viscga, cpga, condga, Rega, Prga
	REAL dum, xDBv, hga, dum0, hDBga, htc
	ENUM Phase phase
	INTEGER ier, ipx,ipy,i
	REAL rho,vsound1
	REAL dum3[23]
	INTEGER dum4[3]
BODY
			i = 1
			dum0 = 0
			xDBv = 1.1 -- vapour fraction where DFFB ends
			c[1] = -0.0179
			c[2] = 1.0092
			c[3] = -0.3130
			c[4] = 0.0325
			c[5] = 0.0640
		IF (x > 1)  THEN
			c[6] = 0.795
		ELSE
			c[6] = 0.8565		
		END IF
			Frfo = G**2/(rhof*Dh*Grav)
			xa   = (c[1]+c[2]*x+c[3]*x**2+c[4]*x**3)*Frfo**c[5]
			xint = NRxa(0.1,c,Frfo)
			PRINT("x=$x,xa=$xa,xint=$xint")
			IF (xa > x) THEN
				xa = x
			ELSEIF (xa >= 1) THEN
				xa = 1
			ELSEIF (xa <= xint) THEN
				xa = x
			END IF
			
			hga = hg + (x-xa)/xa*hfg

			FL_state_vs_ph(mix, x_mix, fluid , fluid_nc, P, hga, 0., dum3[23], phase, rho, dum3[2], dum3[3], dum3[4], dum3[5], dum3[6], dum3[7], dum3[8],dum3[9],
							dum3[10],cpga,dum3[12],dum3[13],dum3[14],dum3[15], vsound1,viscga,dum3[17],dum3[18],
							condga,dum3[20],dum3[21],dum3[22], 1 ,dum4[1], dum4[2], dum4[3])

			Prga = viscga*cpga/condga
			Rega = G*Dh*min(xa,1)/viscga
			hDBga = 0.023*Rega**0.8*Prga**0.4*condga/Dh
			IF (x > xDBv) THEN
				htc = hDBga
			ELSE
				htc = (c[6]+(1-c[6]*(x-1)/(xDBv-1)))*hDBga
				htc = c[6]*hDBga
				PRINT("htc=$htc")
			END IF
			
        RETURN htc

END FUNCTION 


FUNCTION REAL qCHFUCC(
	IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
	--geometry
	IN REAL A					UNITS u_m2		"cross area (m^2)",
	IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	IN REAL z	      		UNITS u_m      "local axial abscissa",
	IN REAL L					UNITS u_m      "tube length",
	--fluid properties
	IN ENUM Phase phase		UNITS no_units	"Fluid phase (-)",
	IN REAL P					UNITS u_Pa		"pressure (Pa)",
	IN REAL T					UNITS u_K		"fluid temperature (K)",
	IN REAL Tsat				UNITS u_K		"saturation temperature (K)",
	IN REAL x					UNITS no_units	"quality",
	IN REAL hf					UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg					UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
	IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
	IN REAL xin					UNITS no_units "inlet quality (-)", 
	IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	IN REAL rhog				UNITS u_kg_m3	"density of vapor (kg/m^3)",
	IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
	IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
	IN REAL kf					UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
	IN REAL kg					UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp					UNITS u_J_kgK	"specific heat (J/kg*K)",
	IN REAL cpf					UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
	IN REAL cpg					UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	IN REAL sigma				UNITS u_N_m		"Surface tension (N/m)",
	--wall properties
	IN REAL Tw					UNITS u_K		"wall temperature (K)",
	IN REAL q					UNITS u_W_m2   "Heat flux",
	IN REAL LE,
	IN REAL Y,
	IN REAL xeine
)
DECLS
	REAL hfg, G, n, qCHF
BODY
	  IF (Y<=1e4) THEN
			n = 0
		ELSEIF ((Y>1E4) AND (Y<1E6)) THEN
			n = (Dh/(LE+1e-6))**(0.54)
		ELSE
			n = 0.12/(1-xeine)*(0.5)
		END IF
		G = m/A
		hfg = hg-hf
		qCHF = G*hfg*0.124*(Dh/LE)**(0.89)*(1e4/Y)**(n)*(1-xeine)
		PRINT("qUCC=$qCHF")
		RETURN qCHF
END FUNCTION


FUNCTION REAL qCHFLCC(
	IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
	--geometry
	IN REAL A					UNITS u_m2		"cross area (m^2)",
	IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	IN REAL z	      		UNITS u_m      "local axial abscissa",
	IN REAL L					UNITS u_m      "tube length",
	--fluid properties
	IN ENUM Phase phase		UNITS no_units	"Fluid phase (-)",
	IN REAL P					UNITS u_Pa		"pressure (Pa)",
	IN REAL T					UNITS u_K		"fluid temperature (K)",
	IN REAL Tsat				UNITS u_K		"saturation temperature (K)",
	IN REAL x					UNITS no_units	"quality",
	IN REAL hf					UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg					UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
	IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
	IN REAL xin					UNITS no_units "inlet quality (-)", 
	IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	IN REAL rhog				UNITS u_kg_m3	"density of vapor (kg/m^3)",
	IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
	IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
	IN REAL kf					UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
	IN REAL kg					UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp					UNITS u_J_kgK	"specific heat (J/kg*K)",
	IN REAL cpf					UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
	IN REAL cpg					UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	IN REAL sigma				UNITS u_N_m		"Surface tension (N/m)",
	--wall properties
	IN REAL Tw					UNITS u_K		"wall temperature (K)",
	IN REAL q					UNITS u_W_m2   "Heat flux",
	IN REAL LE,
	IN REAL Y,
	IN REAL xeine,
	IN REAL Pred,
	IN REAL xein
)
DECLS
	REAL hfg, G, n, qCHF, Boxe, Fe, Fx
	REAL xeCHF 
	REAL B1,B2,B3,B12,Bo,Grav,c,F1,F2,F3

BODY
		G = m/A
		hfg = hg-hf
		B1 = 0.082*Y**(-0.3)*(1+1.45*Pred**(4.03))
		B2 = 0.0024*Y**(-0.105)*(1+1.15*Pred**(3.39))
		B3 = 15*Y**(-0.612)
		B12 = max(B1,B2)
		Boxe = max(B12,B3)
		Bo = q/(G*hfg)
		Grav = 9.81
		xeCHF = xein + 4*Bo*z/Dh
		IF (Pred<=0.6) THEN
			c = 0
		ELSE
			c = 1
		END IF
		F3 = (1.25e5/Y)**(0.833*xeCHF)
		IF (xeCHF>0) THEN
			Fx = F3*(1+(F3*(-0.29)-1)*(Pred-0.6)/0.35)**c
		ELSE
			F1 = 1+0.0052*(-xeCHF**(0.88))*(min(1.4e7,Y))**(0.41)
			IF (F1<=4) THEN
				F2 = F1**(-0.42)
			ELSE
				F2 = 0.55
			END IF
			Fx = F1*(1-(1-F2)*(Pred-0.6)/0.35)**c
		END IF
		Fe = max(1, (1.54-0.032*z/Dh))
		qCHF = G*hfg*Fe*Fx*Boxe
		PRINT("qLCC=$qCHF")
		RETURN qCHF
END FUNCTION


FUNCTION REAL compCHF(
	IN ENUM OPT_CHF   		option_CHF     "Heat transfer option - Critical heat flux", 
	IN ENUM FluidKeys fluid, 
	IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
	--geometry
	IN REAL A					UNITS u_m2		"cross area (m^2)",
	IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	IN REAL z	      		UNITS u_m      "local axial abscissa",
	IN REAL L					UNITS u_m      "tube length",
	--fluid properties
	IN ENUM Phase phase		UNITS no_units	"Fluid phase (-)",
	IN REAL P					UNITS u_Pa		"pressure (Pa)",
	IN REAL T					UNITS u_K		"fluid temperature (K)",
	IN REAL Tsat				UNITS u_K		"saturation temperature (K)",
	IN REAL x					UNITS no_units	"quality",
	IN REAL hf					UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg					UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
	IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
	IN REAL xin					UNITS no_units "inlet quality (-)", 
	IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	IN REAL rhog				UNITS u_kg_m3	"density of vapor (kg/m^3)",
	IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
	IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
	IN REAL kf					UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
	IN REAL kg					UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp					UNITS u_J_kgK	"specific heat (J/kg*K)",
	IN REAL cpf					UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
	IN REAL cpg					UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	IN REAL sigma				UNITS u_N_m		"Surface tension (N/m)",
	--wall properties
	IN REAL Tw					UNITS u_K		"wall temperature (K)",
	IN REAL q					UNITS u_W_m2   "Heat flux",
	OUT REAL xCHF				UNITS no_units,
	OUT STRING CHFtype		
)

DECLS
	REAL htc					UNITS u_W_m2K	"Heat transfer coefficient (W/m^2*K)"
  REAL zCHF					UNITS u_m      "critical length"
  REAL Bo, Wefo, Frfo
  REAL G
  REAL hfg
  REAL c1,c2,c3,c4,c5,c6
  REAL aCHF
  REAL Grav, Re, Pr, Reg, Prg, TwDBge, dTw, Bost, xlim, Y, qCHF, LE,xein,xeine,Pred, Pcrit
	REAL qCHF1, qCHF2
	REAL prop_crit[20]			  "Fluid properties at critical point"
	INTEGER ig, ier
BODY
		G = m/A
		hfg = hg-hf
		Bo = q/(G*hfg)
		Grav = 9.81
		IF (option_CHF == CHF_Ganesan) THEN
		
			Wefo = G**2*Dh/(sigma*rhof)
	
	-- assume that CHF is DNB
	-- if vertical flow
			c1 = 0.19
			c2 = -0.22
			c3 = -0.29
			c4 = 1.11
			c5 = 0.57
			zCHF = Dh*(c1*Wefo**c2*(rhof/rhog)**c3*(1-xin)**(1+c4)/(4*Bo))**(1/(1-c5)) -- modello CHF di Ganesan
			xCHF = xin + 4*Bo*zCHF/Dh
			--PRINT("zchf=$zCHF, Wefo=$Wefo,Bo=$Bo")
			IF (xCHF < 0) THEN
				-- CHF is DNB
				-- valid if pressure is almost constant in tube
				CHFtype = "DNB"
			ELSE   --
				-- evaluate aCHF
				aCHF = (1+(1-xCHF)/xCHF*(rhog/rhof)**(2/3))**(-1)
				IF (aCHF < 0.6) THEN -- Ganesan
					-- DNB 
					CHFtype = "DNB"
				ELSE 
				  --  dryout
				  c1 = 0.85
				  c2 = -0.22
				  c3 = -0.22
				  c4 = 1.83
				  c5 = 0.22
				  zCHF = Dh*(c1*Wefo**c2*(rhof/rhog)**c3*(1-xin)**(1+c4)/(4*Bo))**(1/(1-c5))
				  xCHF = xin + 4*Bo*zCHF/Dh
				  aCHF = (1+(1-xCHF)/xCHF*(rhog/rhof)**(2/3))**(-1)
				 -- ASSERT (aCHF < 0.6) WARNING "aCHF < 0.6 in dryout"
				  CHFtype = "DRY"
				END IF
			END IF
			
		ELSEIF (option_CHF == CHF_Shah) THEN
		CHFtype = "DNB"
					Y = (G*Dh*cpf/kf)*(G**2/(rhof**2*Grav*Dh))**(0.4)*(viscf/viscg)**(0.6)
					IF (Y<=1e6) THEN
						IF (xein<=0) THEN
							LE = z
							xeine = xein
						ELSE
						  LE = z + xein*Dh/(4*Bo)
							xeine = 0
						END IF
							qCHF = qCHFUCC(m, A, Dh, z, L, \
							phase, P, T, Tsat, x, hf, hg, h, xin, rhof, rhog, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, \
							Tw, q, LE, Y, xeine)
							IF (q<qCHF) THEN
								zCHF = z*1.01
							ELSE
								zCHF = z*0.99
							END IF
					ELSE
						IF (xein<=0) THEN
							LE = z
							xeine = xein
						ELSE
							LE = z + xein*Dh/(4*Bo)
							xeine = 0
						END IF
							
					  FL_table_read(fluid, ig)
						crit_cond  (ig, prop_crit, ier)
						Pcrit = prop_crit[1]
						Pred = P/Pcrit
						IF (LE>160/(Pred**1.14)) THEN
							qCHF = qCHFUCC(m, A, Dh, z, L, \
							phase, P, T, Tsat, x, hf, hg, h, xin, rhof, rhog, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, \
							Tw, q, LE, Y, xeine)
						ELSE
							qCHF1 = qCHFUCC(m, A, Dh, z, L, \
							phase, P, T, Tsat, x, hf, hg, h, xin, rhof, rhog, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, \
							Tw, q, LE, Y, xeine)
							qCHF2 = qCHFLCC(m, A, Dh, z, L, \
							phase, P, T, Tsat, x, hf, hg, h, xin, rhof, rhog, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, \
							Tw, q, LE, Y, xeine,Pred,xein)
							qCHF = min(qCHF1,qCHF2)
							IF (q<qCHF) THEN
								zCHF = z*1.01
							ELSE
								zCHF = z*0.99
							END IF
						END IF
					
		END IF
		END IF
		RETURN zCHF
END FUNCTION





FUNCTION REAL htc_hendricks
	 (SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
    IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
    IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
    IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
    IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
    IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
    IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T, 
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
	REAL ReF, PrF, NuF, XttF, rhoF, viscF, cpF, kF, vel
BODY



	prop_vs_T(fluid, 0.5*(Tw+T), P, rhoF, viscF, cpF, kF)
	
	vel = G/rho
	XttF = (viscf/viscF)**(0.1)
	XttF = XttF*(rhoF/rhof)**(0.5)
	XttF = XttF*((1-x)/(max(x,1e-3)))**(0.9)
	IF (x<1) THEN
		IF (x<0) THEN
			rhoF = rhof
		ELSE
			rhoF = 1/(x/rhoF+(1-x)/rhof)
		END IF
	ELSE
		rhoF = rhog
	END IF
	ReF = rhoF*vel*Dh/viscF
	PrF = viscF*cpF/kF



	NuF = 0.023*ReF**(0.8)*PrF**(0.4)
	htc = kF/Dh*NuF/(0.611+1.93*XttF)
	
	PRINT("XttF=$XttF,rhoF=$rhoF,viscF=$viscF,cpF=$cpF,kF=$kF,rho=$rho,G=$G,vel=$vel, rhof=$rhof,viscf=$viscf,x=$x,PrF=$PrF")
	RETURN htc
END FUNCTION



FUNCTION REAL htc_bromley
	 (SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
    IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
    IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
    IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
    IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
    IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
    IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T, 
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
	REAL dT, Lh
BODY
 	dT = abs(Tw-Tsat)
	Lh = min(Dh/2,2*PI*sqrt(hfg/(Grav*(rhof-rhog))))
	htc = 0.62*(kg**3*rhog*Grav*(rhof-rhog)*(hfg+0.4*cpg*dT)/(Lh*viscg*dT))**(1/4)
	RETURN htc
END FUNCTION

FUNCTION REAL htc_bromleymod
	 (SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
    IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
    IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
    IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
    IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
    IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
    IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T, 
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
	REAL dT, h1, Prg
BODY
 	dT = abs(Tw-Tsat)
	Prg = viscg*cpg/kg
 	h1 = 0.62*(kg**2*rhog*Grav*(rhof-rhog)*(hfg+0.4*cpg*dT)/(Dh*dT*Prg))**(1/4)
	htc = 2*h1*(1-1.04*x**2)*(1+0.2*abs(G)**0.1)
	RETURN htc
END FUNCTION


FUNCTION REAL htc_miropolskii
	 (SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
   IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
   IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
   IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
   IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
   IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
   IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T, 
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
	REAL Remix, Y, Prg
BODY
 	Remix = abs(G)*Dh/viscg*(x+(rhog/rhof))*(1-x) -- occhio
	Y=max(0,1-0.1*(rhof/rhog-1)**0.4*(1-x)**0.4)
	Prg = cpg*viscg/kg
 	htc = 0.023*Remix**0.8*Prg**0.4*Y*kg/Dh
	RETURN htc
END FUNCTION


FUNCTION REAL htc_groeneveld
	 (SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin				UNITS no_units "inlet quality (-)", 
 	 IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg					UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf			UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg			UNITS u_Pas		"viscosity of vapor (Pa*s)",
   IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
   IN REAL kf					UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
   IN REAL kg					UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
   IN REAL cp					UNITS u_J_kgK	"specific heat (J/kg*K)",
   IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
   IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T, 
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
	REAL Remix, Y, Prg
BODY
 	Remix = abs(G)*Dh/viscg*(x+(rhog/rhof)*(1-x))
	Y=max(0,1-0.1*(rhof/rhog-1)**0.4*(1-x)**0.4)
	Prg = cpg*viscg/kg
 	htc = 0.00109*Remix**0.989*Prg**1.41*Y**(-1.15)*kg/Dh
	RETURN htc
END FUNCTION


FUNCTION REAL htp_dry
	(	
	 IN ENUM OPT_DRY   		option_DRY     "Heat transfer option - dryout heat flux", 
	 SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
    IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
    IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
    IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
    IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
    IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
    IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T,
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z
	 )
DECLS
	REAL htc
BODY
	IF (option_DRY == DRY_Ganesan) THEN
	
		htc = htpa(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,h,hg,hfg,rhof,sigma,Grav)
		
	ELSEIF (option_DRY == DRY_Hendricks) THEN
	
		htc = htc_hendricks(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,q,z)
		
	ELSEIF (option_DRY == DRY_bromley) THEN
	
		htc = htc_bromley(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		
	ELSEIF (option_DRY == DRY_bromleymod) THEN
	
		htc = htc_bromleymod(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		
	ELSEIF (option_DRY == DRY_miropolskii) THEN
	
		htc = htc_miropolskii(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		
	ELSEIF (option_DRY == DRY_groeneveld) THEN
	
		htc = htc_groeneveld(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		
	ELSE
		htc = 1
	END IF
	RETURN htc
END FUNCTION


FUNCTION REAL htp_dnb
	(	
	 IN ENUM OPT_DNB   		option_DNB     "Heat transfer option - dryout heat flux", 
	 SET_OF(Chemicals_full) mix "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
	 IN REAL x_mix[mix], 
	 IN ENUM FluidKeys fluid, 
	 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
	 IN REAL x					UNITS no_units	"quality",
	 IN REAL Dh					UNITS u_m		"hydraulic diameter (m)",
	 IN REAL G					UNITS no_units "mass flux",
	 IN REAL P					UNITS u_Pa		"pressure (Pa)",
	 IN REAL xin					UNITS no_units "inlet quality (-)", 
 	 IN REAL h				UNITS u_J_kg	"Enthalpy  (J/kg)",
	 IN REAL hg				UNITS u_J_kg	"saturated vapour Enthalpy  (J/kg)",
	 IN REAL hfg				UNITS u_J_kg	"latent heat of vaporization  (J/kg)",
	 IN REAL rho,
	 IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
	 IN REAL rhog,
	 IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
	 IN REAL Grav				UNITS u_m2_s	"gravity acceleration",
	 IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
	 IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
	 IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
    IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
    IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
    IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
    IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
    IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
    IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
	 IN REAL Tw					UNITS u_K		"wall temperature (K)",
	 IN REAL T,
	 IN REAL Tsat,
	 IN REAL q					UNITS u_W_m2   "Heat flux",
	 IN REAL z,
	 OUT REAL flag,
	 OUT REAL dTw
	 )

DECLS
	REAL htc
  REAL xCHF, Re, Pr, Reg, Prg, TwDBge, Bost, xlim
  REAL coeff, beta0, htcCHEN, htcIAFB, htcDFFB
	REAL htc1, htc2, flag1, flag2
BODY




	IF (option_DNB == DNB_Ganesan_neq) THEN 
		Reg = G*Dh/viscg*max(x,1e-3)
		Prg = viscg*cpg/kg
		TwDBge = Tsat + q/(0.023*Reg**0.8*Prg**0.4*kg/Dh)
		dTw = (Tw-Tsat)/(TwDBge-Tsat)

		-- per dTw < 1
	--	htc1 = htpa(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,h,hg,hfg,rhof,sigma,Grav)
	--	flag1 = 4		
		-- per dTw >1
--		Bost = (max(x,1e-3)-min(xin,0.999))/(1-min(xin,0.999))
	--	htc2 = max(100,0.7484*0.023*Reg**0.8*Prg**0.4*kg/Dh*(Bost)**(-0.4133))
	--	flag2 = 5
	--	coeff = 0.5*(1 + tanh(5*(dTw-1)))
	--	htc = coeff*htc1+(1-coeff)*htc2
	--	flag = coeff*flag1+(1-coeff)*flag2
		--PRINT("coeff=$coeff,flag=$flag")
		
	--	IF (dTw > 1) THEN
			flag = 4
			htc = htpa(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,h,hg,hfg,rhof,sigma,Grav)
--		ELSE
--			Bost = (max(x,1e-3)-min(xin,0.999))/(1-min(xin,0.999))
--			flag = 5
--			htc = max(100,0.7484*0.023*Reg**0.8*Prg**0.4*kg/Dh*(Bost)**(-0.4133))
--		END IF
		
	ELSEIF (option_DNB == DNB_Ganesan_eq) THEN
	
		Reg = G*Dh/viscg*max(x,1e-3)
		Prg = viscg*cpg/kg
		TwDBge = Tsat + q/(0.023*Reg**0.8*Prg**0.4*kg/Dh)
		dTw = (Tw-Tsat)/(TwDBge-Tsat)
		Bost = (max(x,1e-3)-min(xin,0.999))/(1-min(xin,0.999))

	--	IF (dTw > 1) THEN
			htcDFFB = max(100,0.5236*0.023*Reg**0.8*Prg**0.4*kg/Dh*(Bost)**(-0.3243))
			htc=htcDFFB
			flag = 4
			
	--	ELSE			
	--		htcIAFB = max(100,0.7484*0.023*Reg**0.8*Prg**0.4*kg/Dh*(Bost)**(-0.4133))
	--		htc = htcIAFB	
	--		flag = 5
	--	END IF		


		
	ELSEIF (option_DNB == DNB_Hendricks) THEN
	
			htc = htc_hendricks(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,q,z)
			flag = 4
	ELSEIF (option_DNB == DNB_bromley) THEN
	
		htc = htc_bromley(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		flag = 4
	ELSEIF (option_DNB == DNB_bromleymod) THEN
	
		htc = htc_bromleymod(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		flag = 4
	ELSEIF (option_DNB == DNB_miropolskii) THEN
	
		htc = htc_miropolskii(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		flag = 4
	ELSEIF (option_DNB == DNB_groeneveld) THEN
	
		htc = htc_groeneveld(mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)
		flag = 4
	ELSE
	
		htc = 1
		
	END IF

		
		


	RETURN htc
END FUNCTION


FUNCTION REAL htp_nucleate
   (
		  IN ENUM OPT_NUC  option_NUC     "Heat transfer option - nucleate heat flux", 
        IN REAL m			UNITS u_kg_s	"Total mass flow (kg/s)",
        IN REAL xlim			UNITS no_units	"Fraction of vapor flow (-)",
        --
        IN REAL A			UNITS u_m2		"Cross area (m^2)",
        IN REAL Dh		UNITS u_m		"hydraulic diameter (m)",
        --
        IN REAL P			UNITS u_Pa		"pressure (Pa)",
        IN REAL T			UNITS u_K		"fluid temperature (K)",
        IN REAL Tsat		UNITS u_K		"saturation temperature (K)",
        IN REAL hf		UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
        IN REAL hg		UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
        --
        IN REAL kf		UNITS u_W_mK	"Thermal conductivity of liquid (W/m*K)",
        IN REAL rhof		UNITS u_kg_m3	"Density of liquid (kg/m^3)",
        IN REAL rhog		UNITS u_kg_m3	"Density of vapor (kg/m^3)",
        IN REAL viscf	UNITS u_Pas		"Viscosity of liquid (Pa*s)",
        IN REAL viscg	UNITS u_Pas		"Viscosity of vapor (Pa*s)",
        IN REAL cpf		UNITS u_J_kgK	"Specific heat of liquid (J/kg*K)",
        IN REAL sigma	UNITS u_N_m		"Surface tension (N/m)",
        IN REAL Tw		UNITS u_K		"Wall temperature (K)"
    )
DECLS
	REAL htc
BODY

	IF (option_NUC == NUC_Chen) THEN
			htc = htc_tube_boiling_Chen(m, xlim, A, Dh, \
         P, T, Tsat, hf, hg, kf, rhof, rhog, viscf, viscg, cpf, sigma, Tw)
	ELSE
		htc = 1
	END IF

	RETURN htc
END FUNCTION



-- Function computes the pool boiling heat transfer coefficient
 FUNCTION REAL htc_boiling(
 			IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
        IN REAL A						UNITS u_m2		"Cross area (m^2)",
        IN REAL Dh					UNITS u_m		"Hydraulic diameter (m)",
         --fluid properties
        IN REAL p						UNITS u_Pa		"pressure (Pa)",
        IN REAL T						UNITS u_K		"fluid temperature (K)",
        IN REAL Tsat					UNITS u_K		"saturation temperature (K)",
		  IN REAL Tcr					UNITS u_K      "Critical temperature (K)",
        IN REAL hf					UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
        IN REAL hg					UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
        IN REAL rho					UNITS u_kg_m3	"fluid density (kg/m^3)",
        IN REAL rhof					UNITS u_kg_m3	"density of liquid (kg/m^3)",
        IN REAL rhog					UNITS u_kg_m3	"density of vapor (kg/m^3)",
        IN REAL x						UNITS no_units	"quality (-)",
        IN REAL visc					UNITS u_Pas		"viscosity  (Pa*s)",
        IN REAL viscf				UNITS u_Pas		"viscosity of liquid (Pa*s)",
        IN REAL viscg				UNITS u_Pas		"viscosity of vapor (Pa*s)",
        IN REAL k						UNITS u_W_mK	"thermal conductivity  (W/m*K)",
        IN REAL kf					UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
        IN REAL kg					UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
        IN REAL cp					UNITS u_J_kgK	"specific heat (J/kg*K)",
        IN REAL cpf					UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
        IN REAL cpg					UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
        --wall properties
        IN REAL sigma				UNITS u_N_m		"Surface tension (N/m)",
        IN REAL Tw					UNITS u_K		"wall temperature (K)",
        IN REAL Grav					UNITS u_m_s2	"Gravitational acceleration (m/s^2)"
		  )
 	DECLS

	
		REAL htc
	
	
		REAL dh,dT,drho,L_b,Pr_l,Pr_v
		REAL Ra,Ja,Ph
		REAL q_ch,dT_ch
		REAL K, q_cr,T_cr
		REAL q_mi,Nu_fb,T_fb
	        
	   REAL Csf = 0.015	UNITS no_units	"Adjusting constant for nucleate boiling (-)"
	   REAL r = 0.4		UNITS no_units	"Adjusting exponent for nucleate boiling (-)"
	
	BODY
	
	IF(T>=Tsat OR Tw<=Tsat OR T>Tsat OR sigma<1e-6) THEN         -- No boiling
   	htc = htc_tube_sp(m, A, Dh, k, visc, cp)
   ELSE
     dh = max(1e-3, hg - hf)
     dT = Tw - Tsat
     drho = rhof - rhog

 --  L_b  = sqrt(sigma/Grav/drho)           -- capillary length
     L_b  = max( 1e-6, 1.8*sqrt(sigma/max(1e-4, Grav*rhof)) )
     Pr_l = cpf * viscf / kf
     Pr_v = cpg * viscg / kg

     Ra = abs(Grav * rhog * drho) * Dh**3 * Pr_v / viscg**2
     Ja = cpg * abs(dT) / dh
     Ph = cpf *(Tsat-T) / dh               -- Phase nb.

     q_ch  = viscf*dh/L_b                   -- Characteristic capillary flux
     dT_ch = Csf*dh/cpf*Pr_l**1.7           -- Characteristic dT

     -- Maximum heat flux
     K = 0.16*(1 + Ph*0.065*(rhof/rhog)**0.8)   -- Kutateladze correlation
     q_cr = K*dh*sqrt(rhog)*(sigma*drho*Grav)**0.25
     T_cr = Tsat + dT_ch * (q_cr/q_ch)**r

     -- Minimum heat flux
     q_mi = 0.09*dh*rhog*(sigma*drho*Grav/(rhof+rhog)**2)**0.25   -- Zuber correlation
     Nu_fb = 0.425*(Dh/L_b*Ra/Ja)**0.25
     T_fb  = T_cr + q_mi*Dh/kg/Nu_fb
 --  PRINT("     T_cr=$T_cr T_fb=$T_fb Pr_l=$Pr_l q_cr=$q_cr q_mi=$q_mi q_ch=$q_ch K=$K Ph=$Ph dh=$dh dT_ch=$dT_ch")

     IF(Tw < T_cr) THEN        -- Nucleate boiling
     		htc = q_ch *fpow(dT/dT_ch, 0.001, 1/r) / abs(T-Tw)
     ELSE
       ASSERT(T_fb > T_cr) FATAL "The film boiling temperature is lesser than Tcrit_heat_flux"

       IF(Tw > T_fb) THEN      -- Film boiling
          htc = kg/Dh * Nu_fb * dT / abs(T-Tw)
       ELSE                    -- Transition boiling
          htc = (q_cr + (q_mi-q_cr)/(T_fb-T_cr)*(Tw-T_cr)) / abs(T-Tw)
       END IF
     END IF
   END IF
	
	RETURN htc
	
 END FUNCTION


FUNCTION REAL htc_tube_twophase
    (
	 	 SET_OF(Chemicals_full) mix  "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
		 IN REAL x_mix[mix], 
		 IN ENUM FluidKeys fluid, 
		 IN ENUM FluidKeys fluid_nc "Non-condensable working fluid",
		 IN ENUM OPT_GEN			option_gen		" general option to use legacy correlations",
		 IN ENUM COND_Option  cond_flag     " condensation flag from HT_tube model",
		 IN ENUM OPT_CHF   		option_CHF     "Heat transfer option - Critical heat flux", 
		 IN ENUM OPT_DRY   		option_DRY     "Heat transfer option - Dryout heat flux", 
		 IN ENUM OPT_DNB   		option_DNB     "Heat transfer option - DNB heat flux",
		 IN ENUM OPT_NUC   		option_NUC     "Heat transfer option - NUC heat flux",
	   --mass flow
	   IN REAL m					UNITS u_kg_s	"Average mass flow through volume (kg/s)",
	   --geometry
	   IN REAL A					UNITS u_m2		"cross area (m^2)",
	   IN REAL Dh				UNITS u_m		"hydraulic diameter (m)",
		 IN REAL z	      		UNITS u_m      "local axial abscissa",
		 IN REAL L					UNITS u_m      "tube length",
     --fluid properties
     IN ENUM Phase phase	UNITS no_units	"Fluid phase (-)",
     IN REAL P					UNITS u_Pa		"pressure (Pa)",
     IN REAL T					UNITS u_K		"fluid temperature (K)",
     IN REAL Tsat				UNITS u_K		"saturation temperature (K)",
		 IN REAL x					UNITS no_units	"quality",
     IN REAL hf				UNITS u_J_kg	"Enthalpy of saturated liquid (J/kg)",
     IN REAL hg				UNITS u_J_kg	"Enthalpy of saturated vapor  (J/kg)",
		 IN REAL h					UNITS u_J_kg	"Enthalpy  (J/kg)",
		 IN REAL xin				UNITS no_units "inlet quality (-)", 
		 IN REAL rho,
     IN REAL rhof				UNITS u_kg_m3	"density of liquid (kg/m^3)",
     IN REAL rhog				UNITS u_kg_m3	"density of vapor (kg/m^3)",
     IN REAL visc				UNITS u_Pas		"viscosity  (Pa*s)",
     IN REAL viscf			UNITS u_Pas		"viscosity of liquid (Pa*s)",
     IN REAL viscg			UNITS u_Pas		"viscosity of vapor (Pa*s)",
     IN REAL k					UNITS u_W_mK	"thermal conductivity  (W/m*K)",
     IN REAL kf				UNITS u_W_mK	"thermal conductivity of liquid (W/m*K)",
     IN REAL kg				UNITS u_W_mK	"thermal conductivity of vapor  (W/m*K)",
     IN REAL cp				UNITS u_J_kgK	"specific heat (J/kg*K)",
     IN REAL cpf				UNITS u_J_kgK	"specific heat of liquid (J/kg*K)",
     IN REAL cpg				UNITS u_J_kgK	"specific heat of vapor (J/kg*K)",
     IN REAL sigma			UNITS u_N_m		"Surface tension (N/m)",
     --wall properties
     IN REAL Tw				UNITS u_K		"wall temperature (K)",
		 IN REAL q					UNITS u_W_m2   "Heat flux",
		 OUT REAL flag			UNITS no_units "flag for regime identification",
		 OUT REAL dTw
        )

    DECLS
      REAL htc					UNITS u_W_m2K	"Heat transfer coefficient (W/m^2*K)"
		  REAL zCHF					UNITS u_m      "critical length"
		  REAL Bo, Wefo, Frfo
		  REAL G
		  REAL hfg
		  REAL c1,c2,c3,c4,c5,c6
		  REAL aCHF
		  STRING CHFtype
		  REAL xCHF, Grav, Re, Pr, Reg, Prg, TwDBge, Bost, xlim
		  REAL coeff, beta0, htcCHEN, htcIAFB, htcDFFB,Tcr
    BODY

		IF (option_gen == HT_tube_orig) THEN
		 htc = htc_tube(cond_flag, m, A, Dh, phase, P, T, Tsat, hf, hg, x, rhof, rhog,
                           visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, Tw)
				RETURN htc
		ELSEIF (option_gen == HT_boil_orig) THEN
			Tcr = Tcr_comp(fluid)
			htc = htc_boiling(m,A,Dh,P,T,Tsat,Tcr,hf,hg,rho,rhof,rhog,x,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,sigma,Tw,Grav)
				RETURN htc
		END IF
--    preliminary calculations	 
	 	xlim = x
		x = (h-hf)/(hg-hf)		
		Grav = 9.80665
		G = m/A
		
--		If Tw<Tsat use liquid heat transfer coefficient
		IF (Tw<Tsat) THEN
			Re = G*Dh/visc
			Pr = visc*cp/k
			htc = k/Dh*0.023*Re**0.8*Pr**0.4
			flag = 1
			RETURN htc
		END IF
		
-- determine zCHF for which q is critical		
		
		zCHF = compCHF(option_CHF, fluid, m, A, Dh, z, L, \
				phase, P, T, Tsat, x, hf, hg, h, xin, rhof, rhog, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, \
				Tw, q, xCHF, CHFtype)
		
		hfg = hg-hf
		Bo = q/(G*hfg)
		Wefo = G**2*Dh/(sigma*rhof)

		PRINT("zchf=$zCHF, CHFtype=$CHFtype")
		--ASSERT (zCHF > 0) WARNING "zCHF < 0"
		-- 
--		IF (zCHF<=0) THEN
--				Re = G*Dh/visc
--				Pr = visc*cp/k
--				htc = k/Dh*0.023*Re**0.8*Pr**0.4
--				flag = 6
--				--PRINT("db Z<0 hc = $htc")
--				RETURN htc
--		END IF



		
		IF ((z > zCHF) AND (CHFtype == "DRY")) THEN
		
			htc = htp_dry(option_DRY,mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z)

			flag = 3

		ELSEIF ((z > zCHF) AND (CHFtype == "DNB")) THEN
		
			htc = htp_dnb(option_DNB,mix,x_mix,fluid,fluid_nc,x,Dh,G,P,xin,h,hg,hfg,rho,rhof,rhog,sigma,Grav,visc,viscf,viscg,k,kf,kg,cp,cpf,cpg,Tw,T,Tsat,q,z,flag,dTw)
			
		ELSE -- if z<zCHF	nucleate boiling
				
			htc = htp_nucleate(option_NUC,m, xlim, A, Dh, \
         P, T, Tsat, hf, hg, kf, rhof, rhog, viscf, viscg, cpf, sigma, Tw)
			flag = 2
			--PRINT("CHEN hc = $htc")

		END IF
		
		

		--htc = abs(htc)
		--PRINT("final hc = $htc")

        RETURN htc

END FUNCTION

